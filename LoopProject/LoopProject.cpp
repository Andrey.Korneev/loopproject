#include <iostream>

void PrintNumbers(int N, bool isEven)
{
    int i = 0;

    if (!isEven) i++;

    for (; i <= N; i += 2)
    {
        std::cout << i << ' ';
    }
}

int main()
{
    int N = 100;

    std::cout << "Print even numbers with a cycle in the main-function (from 0 to " << N << "):" << std::endl;
    for (int i = 0; i <= N; i += 2) {
        std::cout << i << ' ';
    }

    std::cout << std::endl << std::endl;

    std::cout << "Print numbers in function PrintNumbers (from 0 to " << N << "):" << std::endl;
    std::cout << "Even numbers:" << std::endl;
    PrintNumbers(N, true);
    std::cout << std::endl;
    std::cout << "Odd numbers:" << std::endl;
    PrintNumbers(N, false);
    std::cout << std::endl;
}